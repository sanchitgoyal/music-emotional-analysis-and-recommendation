<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Fuzzy Music Emotion Classification & Recommendation</title>
    <?php 
      echo '<link rel="stylesheet" href="'.$_SERVER["PLUGINS_FOUNDATION"].'css/foundation.css" />'; 
      echo '<link rel="stylesheet" href="'.$_SERVER["PLUGINS_DATATABLES"].'css/jquery.dataTables.css" />'; 
      echo '<link rel="stylesheet" href="'.$_SERVER["PLUGINS_DATATABLES"].'css/jquery.dataTables_themeroller.css" />'; 
      echo '<script src="'.$_SERVER["PLUGINS_FOUNDATION"].'js/vendor/modernizr.js"></script>';
      echo '<script src="'.$_SERVER["PLUGINS_FOUNDATION"].'js/vendor/jquery.js"></script>';
      echo '<script src="'.$_SERVER["PLUGINS_FOUNDATION"].'js/foundation.min.js"></script>';
      echo '<script src="'.$_SERVER["PLUGINS_DATATABLES"].'js/jquery.dataTables.min.js"></script>';
    ?>
    
	<style>
		body, html {
		  height: 1000px;
		}
	</style>

    <script>
      $(document).foundation();
    </script>
  </head>

  <body>

    <div class="off-canvas-wrap docs-wrap">
      <div class="inner-wrap" style="height:100%">
        <nav class="tab-bar">
          <section class="left-small">
            <a class="left-off-canvas-toggle menu-icon"><span></span></a>
          </section>

          <section class="right">
            <h2 class="title">M.E.A.R</h2>
          </section>

          <section class="right tab-bar-section">
            <h1 class="title">Music Emotion Analysis & Recommendation</h1>
          </section>
        </nav>

        <aside class="left-off-canvas-menu">
          <ul class="off-canvas-list">
            <li><label>User Feedback</label></li>
            <li><a href="<?php echo $_SERVER['APP_ROOT']; ?>feedback-analysis/tempo.php">Tempo Analysis</a></li>
            <li><a href="<?php echo $_SERVER['APP_ROOT']; ?>feedback-analysis/pitch.php">Pitch Analysis</a></li>
            <li><a href="<?php echo $_SERVER['APP_ROOT']; ?>feedback-analysis/intensity.php">Intensity Analysis</a></li>
            <li><a href="<?php echo $_SERVER['APP_ROOT']; ?>feedback-analysis/valence.php">Valence Analysis</a></li>
            <li><a href="<?php echo $_SERVER['APP_ROOT']; ?>feedback-analysis/emotion.php">Emotion Analysis</a></li>
          </ul>
        </aside>

        <section class="main-section" style="height:inherit">
