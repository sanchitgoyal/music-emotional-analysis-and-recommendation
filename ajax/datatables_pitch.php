<?php

  if($_REQUEST["tableType"] == "music"){
    $aColumns = array( 'idClip', 'filename');
  }
  else if($_REQUEST["tableType"] == "responseStats"){
    $aColumns = array( 'idClip', 'low', "medium", "high", "total");
  }
  else if($_REQUEST["tableType"] == "fuzzyResults"){
    $aColumns = array( 'idClip', 'low', "medium", "high", "total", "pitchMin", "pitchMax");
  }
  else if($_REQUEST["tableType"] == "indivResponses"){
    $aColumns = array( 'idClip', 'idSurveyor','low', 'medium', 'high', 'pitchMin', 'pitchMax' );
  }  
  
  /* Indexed column (used for fast and accurate table cardinality) */
  $sIndexColumn = "idClip";
  
  /* DB table to use */

  if($_REQUEST["tableType"] == "music"){
    $sTable = "(
      SELECT idClip, filename 
      FROM musicemotionsurvey_musicclips
      WHERE true";
    if(isset($_REQUEST["musicGroup"])) {
      $sTable = $sTable. " AND grouping=".$_REQUEST["musicGroup"];
    }      
    $sTable = $sTable. "
    ) as abcd";  
  }
  else if($_REQUEST["tableType"] == "responseStats"){
    $sTable = "
    (
      SELECT idClip, 
            CASE WHEN low IS NULL THEN 0 ELSE low END as low, 
            CASE WHEN medium IS NULL THEN 0 ELSE medium END as medium, 
            CASE WHEN high IS NULL THEN 0 ELSE high END as high, 
            CASE WHEN total IS NULL THEN 0 ELSE total END as total FROM
      (
        SELECT idClip 
        FROM mscsthesis.musicemotionsurvey_musicclips";
        if(isset($_REQUEST["musicGroup"])) {
          $sTable = $sTable. " WHERE grouping=".$_REQUEST["musicGroup"];
        }
        $sTable = $sTable. "
      ) mainMusicTable
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as low
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip, pitchCategory
        having pitchCategory = 'low'
      ) lowResults
      USING (idCLip)
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as medium
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip, pitchCategory
        having pitchCategory = 'medium'
      ) mediumResults
      USING (idCLip)
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as high
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip, pitchCategory
        having pitchCategory = 'high'
      ) highResults
      USING (idCLip)
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as total
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip
      ) totalResults
      USING (idCLip)
    ) as abcd  
    ";
  }
  else if($_REQUEST["tableType"] == "fuzzyResults"){
    $sTable = "
    (
      SELECT idClip, 
              (CASE WHEN low IS NULL THEN 0 ELSE low END)/total as low, 
              (CASE WHEN medium IS NULL THEN 0 ELSE medium END)/total as medium, 
              (CASE WHEN high IS NULL THEN 0 ELSE high END)/total as high, 
              CASE WHEN total IS NULL THEN 0 ELSE total END as total, 
              CASE WHEN pitchMin IS NULL THEN 0 ELSE pitchMin END as pitchMin, 
              CASE WHEN pitchMax IS NULL THEN 0 ELSE pitchMax END as pitchMax
      FROM
      (
        SELECT idClip 
        FROM mscsthesis.musicemotionsurvey_musicclips";
        if(isset($_REQUEST["musicGroup"])) {
          $sTable = $sTable. " WHERE grouping=".$_REQUEST["musicGroup"];
        }
        $sTable = $sTable. "
      ) mainMusicTable
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as low
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip, pitchCategory
        having pitchCategory = 'low'
      ) lowResults
      USING (idCLip)
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as medium
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip, pitchCategory
        having pitchCategory = 'medium'
      ) mediumResults
      USING (idCLip)
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as high
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip, pitchCategory
        having pitchCategory = 'high'
      ) highResults
      USING (idCLip)
      LEFT OUTER JOIN
      (
        SELECT idClip, count(*) as total
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip
      ) totalResults
      USING (idCLip)
      LEFT OUTER JOIN
      (
        SELECT idClip, avg(pitchMin) as pitchMin
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip
      ) minAVG
      USING (idCLip)   
      LEFT OUTER JOIN
      (
        SELECT idClip, avg(pitchMax) as pitchMax
        FROM mscsthesis.musicemotionsurvey_pitchdata 
        Group by idClip
      ) maxAVG
      USING (idCLip)    
    ) as abcd  
    ";
  }

  else if($_REQUEST["tableType"] == "indivResponses"){
    $sTable = "
      (
        SELECT idClip, idSurveyor,
                CASE WHEN STRCMP(pitchCategory,'low') THEN 'yes' ELSE 'no' END as low, 
                CASE WHEN STRCMP(pitchCategory,'medium') THEN 'yes' ELSE 'no' END as medium, 
                CASE WHEN STRCMP(pitchCategory,'high') THEN 'yes' ELSE 'no' END as high, 
                CASE WHEN pitchMin IS NULL THEN 0 ELSE pitchMin END as pitchMin, 
                CASE WHEN pitchMax IS NULL THEN 0 ELSE pitchMax END as pitchMax
        FROM mscsthesis.musicemotionsurvey_pitchdata INNER JOIN mscsthesis.musicemotionsurvey_musicclips 
        USING(idClip)
        WHERE true";
    if(isset($_REQUEST["musicGroup"])) {
      $sTable = $sTable. " AND grouping=".$_REQUEST["musicGroup"];
    }
    if(isset($_REQUEST["clipFilter"])){
      $sTable = $sTable." AND idClip=".$_REQUEST["clipFilter"];
    }    
    if(isset($_REQUEST["surveyorFilter"])){
      $sTable = $sTable." AND idSurveyor=".$_REQUEST["surveyorFilter"];
    }
    $sTable = $sTable."     
      ) as abcd  
    ";
  }  
  
  /* Database connection information */
  $gaSql['user']       = "mscsthesis";
  $gaSql['password']   = "Ph935053882!";
  $gaSql['db']         = "mscsthesis";
  $gaSql['server']     = "mscsthesis.db.11004181.hostedresource.com";  
    
  /* 
   * MySQL connection
   */
  $gaSql['link'] =  mysql_pconnect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) or
    die( 'Could not open connection to server' );
  
  mysql_select_db( $gaSql['db'], $gaSql['link'] ) or 
    die( 'Could not select database '. $gaSql['db'] );
  
  
  /* 
   * Paging
   */
  $sLimit = "";
  if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
  {
    $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
      intval( $_GET['iDisplayLength'] );
  }
  
  
  /*
   * Ordering
   */
  $sOrder = "";
  if ( isset( $_GET['iSortCol_0'] ) )
  {
    $sOrder = "ORDER BY  ";
    for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
    {
      if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
      {
        $sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
          ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
      }
    }
    
    $sOrder = substr_replace( $sOrder, "", -2 );
    if ( $sOrder == "ORDER BY" )
    {
      $sOrder = "";
    }
  }
  
  /* 
   * Filtering
   */
  $sWhere = "";
  if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
  {
    $sWhere = "WHERE (";
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
    }
    $sWhere = substr_replace( $sWhere, "", -3 );
    $sWhere .= ')';
  }
  
  /* Individual column filtering */
  for ( $i=0 ; $i<count($aColumns) ; $i++ )
  {
    if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
    {
      if ( $sWhere == "" )
      {
        $sWhere = "WHERE ";
      }
      else
      {
        $sWhere .= " AND ";
      }
      $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
    }
  }
  
  
  /*
   * SQL queries
   * Get data to display
   */
  $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM   $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
  $rResult = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
  
  /* Data set length after filtering */
  $sQuery = "
    SELECT FOUND_ROWS()
  ";
  $rResultFilterTotal = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
  $aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
  $iFilteredTotal = $aResultFilterTotal[0];
  
  /* Total data set length */
  $sQuery = "
    SELECT COUNT(`".$sIndexColumn."`)
    FROM   $sTable
  ";
  $rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
  $aResultTotal = mysql_fetch_array($rResultTotal);
  $iTotal = $aResultTotal[0];
  
  /*
   * Output
   */
  $output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
  );
  
  while ( $aRow = mysql_fetch_array( $rResult ) )
  {
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $_REQUEST["tableType"] == "music" && $aColumns[$i] == "filename" )
      {
        $row[] = "<audio controls>".
                    "<source src='".$_SERVER["MUSIC_DB_ROOT"]."/".$aRow[ $aColumns[$i] ]."' type='audio/mpeg'>".
                    "Your browser does not support the audio element".
                  "</audio>";
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[] = $aRow[ $aColumns[$i] ];
      }
    }
    $output['aaData'][] = $row;
  }

  echo json_encode( $output );

?>