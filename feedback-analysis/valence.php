<?php
	require $_SERVER["DOCUMENT_ROOT"].$_SERVER["APP_ROOT"]."header.php";
?>

<script type="text/javascript">
	$(document).ready(function() {
		$(".sub-nav dd").click(function(){
			//UI: Toggle Activeness Amongst Filters
			$(".sub-nav dd").removeClass("active");
			$(this).addClass("active");
			//Function: Refresh Results
			$("#music").dataTable().fnDraw();
			$("#responseStats").dataTable().fnDraw();
			$("#fuzzyResults").dataTable().fnDraw();
			$("#indivResponses").dataTable().fnDraw();
		});

		$("#music, #responseStats, #fuzzyResults, #indivResponses").each(function(){			
			$(this).dataTable({
				"bPaginate": false,
				"bFilter": false,
				"bInfo": false,
				"bProcessing": true,
	        	"bServerSide": true,
				"sAjaxSource": '../ajax/datatables_valence.php',
				"fnServerParams": function ( aoData ) {
	            	aoData.push( { "name": "tableType", "value": $(this).attr("id")} );
	            	if($(".sub-nav dd.active").children("a").data("group")!=0){
	            		aoData.push( { "name": "musicGroup", "value": $(".sub-nav dd.active").children("a").data("group") } );
	            	}
	        		if($(this).attr("id")=="indivResponses"){
	        			if($("#clipFilter").children(":selected").html()!="*"){
	        				aoData.push( { "name": "clipFilter", "value": $("#clipFilter").children(":selected").html()} );
	        			}
	        			if($("#surveyorFilter").children(":selected").html()!="*"){
	        				aoData.push( { "name": "surveyorFilter", "value": $("#surveyorFilter").children(":selected").html()} );
	        			}
	        		}
	        	}
			});	
		});	

		$("#clipFilter").change(function(){
			var selectedClip = $(this).children(":selected").html();
			try{
				$("#indivResponses").dataTable().fnDraw();
			}catch(err){alert(err);}
		});

		$("#surveyorFilter").change(function(){
			var selectedSurveyor = $(this).children(":selected").html();
			try{
				$("#indivResponses").dataTable().fnDraw();
			}catch(err){alert(err);}
		});
	} );
</script>

<div class="row">
	<div class="large-4 columns">&nbsp;</div>
	<div class="large-4 columns"><h3>Valence Analysis</h3></div>
	<div class="large-4 columns">&nbsp;</div>
</div>

<br/><br />

<div class="row">
	<form>
		<fieldset class="large-12 columns">
			<legend>Filter</legend>
			<dl class="sub-nav"> 
				<dd class="active"><a href="#" data-group="0">All</a></dd> 
				<dd><a href="#" data-group="1">Group-1</a></dd> 
				<dd><a href="#" data-group="2">Group-2</a></dd> 
				<dd><a href="#" data-group="3">Group-3</a></dd> 
			</dl>		
		</fieldset>
	</form>
</div>

<div class="row"> 

	<fieldset class="large-5 columns">
		<legend>Music</legend>
		<div>
			<table id="music">
				<thead>
					<tr>
						<th></th>
						<th>Music</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>
							
						</td>
					</tr>	
				</tbody>
			</table>
		</div>	
	</fieldset>	

	<fieldset class="large-6 columns">
		<legend>Response Statistics</legend>

		<div>
			<table id="responseStats">
				<thead>
					<tr>
						<th>Clip</th>
						<th>Negative</th>
						<th>Neutral</th>
						<th>Positive</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>1</td>
						<td>2</td>
						<td>3</td>
						<td>6</td>
					</tr>	
				</tbody>
			</table>
		</div>	

	</fieldset>

</div>	

<fieldset class="row">
	<legend>Fuzzification</legend>
	<table id="fuzzyResults">
		<thead>
			<tr>
				<th>Clip</th>
				<th>Negative</th>
				<th>Neutral</th>
				<th>Positive</th>
				<th>Total</th>
				<th>Min Valence</th>
				<th>Max Valence</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>1</td>
				<td>2</td>
				<td>3</td>
				<td>6</td>
				<td>7</td>
			</tr>	
		</tbody>
	</table>
</fieldset>

<br /><br />

<fieldset class="row">
	<legend>Individual Responses</legend>

	<?php
	  /* Database connection information */
	  $gaSql['user']       = "mscsthesis";
	  $gaSql['password']   = "Ph935053882!";
	  $gaSql['db']         = "mscsthesis";
	  $gaSql['server']     = "mscsthesis.db.11004181.hostedresource.com";  

	  /* 
	   * MySQL connection
	   */
	  $gaSql['link'] =  mysql_pconnect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) or
	    die( 'Could not open connection to server' );
	  
	  mysql_select_db( $gaSql['db'], $gaSql['link'] ) or 
	    die( 'Could not select database '. $gaSql['db'] );

	    $clipIDQuery = "SELECT idClip FROM mscsthesis.musicemotionsurvey_musicclips";
	    $SurveyorIDQuery = "SELECT DISTINCT idSurveyor FROM mscsthesis.musicemotionsurvey_valencedata";

  		$clipIDResult = mysql_query( $clipIDQuery, $gaSql['link'] ) or die(mysql_error());
  		$SurveyorIDResult = mysql_query( $SurveyorIDQuery, $gaSql['link'] ) or die(mysql_error());
	?>

	<div>
		<legend>Filter</legend>
		<form>
			<label>Clip ID</label>
			<select id="clipFilter">
				<option>*</option>
				<?php 
					while ($row = mysql_fetch_row($clipIDResult))
					{
					    echo "<option>".$row[0].'</option>';
					}
				?>
			</select>

			<label>Surveyor ID</label>
			<select id="surveyorFilter">
				<option>*</option>
				<?php 
					while ($row = mysql_fetch_row($SurveyorIDResult))
					{
					    echo "<option>".$row[0].'</option>';
					}
				?>
			</select>
		</form>
	</div>

	<table id="indivResponses">
		<thead>
			<tr>
				<th>Clip</th>
				<th>Surveyor ID</th>
				<th>Negative</th>
				<th>Neutral</th>
				<th>Positive</th>
				<th>Min Valence</th>
				<th>Max Valence</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>123</td>
				<td>No</td>
				<td>No</td>
				<td>Yes</td>
				<td>6</td>
				<td>7</td>
			</tr>	
		</tbody>
	</table></fieldset>

<?php
	require $_SERVER["DOCUMENT_ROOT"].$_SERVER["APP_ROOT"]."footer.php";
?>